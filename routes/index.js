const express = require('express');

const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let comments;

  req.app.locals.Comment.find().sort({createdAt: 'desc'}).exec(function (err, docs) {
    comments = docs;

    res.render('index', {
      title: 'Web Guild: Cross-Site Scripting (XSS) Lab',
      comments,
    });
  });
});

router.post('/', function(req, res, next) {
  const comment = new req.app.locals.Comment({
    author: req.body.author,
    comment: req.body.comment,
    createdAt: new Date(),
  })

  comment.save();

  res.redirect('/');

  res.end();
});

module.exports = router;
