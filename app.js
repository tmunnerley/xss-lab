var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var db_username = process.env.XSS_LAB_USERNAME;
var db_password = process.env.XSS_LAB_PASSWORD;

const mongoose = require('mongoose');
mongoose.connect(`mongodb://${db_username}:${db_password}@ds147391.mlab.com:47391/heroku_c7ncfbr2`);
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', console.log.bind(console, 'db connection successful'));

const commentSchema = new mongoose.Schema({
  author: String,
  comment: String,
  createdAt: Date,
});

const Comment = mongoose.model('Comment', commentSchema);

var app = express();

app.locals.Comment = Comment;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
