# Cross-Site Scripting Lab

## Summary
This simple Node app is meant to demonstrate how a website that's vulnerable to XSS might be exploited.
The app is a primitive commenting application where the inputs are not sanitized for HTML/scripts. 

## Running the app locally

Install the dependencies with:

`npm install`

Run the app via:

`npm start`

The app will now be running at http://localhost:3000

## Example Scripts

### Alert

`<script>
    alert('Hello world!');
</script>`

### onload Event

`<div onload="console.log('onload event handler')">`

### setInterval

`<script>
    setInterval(function () {
        console.log('setInterval handler');
    }, 3000);
</script>`

## Resources
[OWASP Cross Site Scripting (XSS)](https://owasp.org/www-community/attacks/xss/)

[OWASP Cross Site Scripting Prevention Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)


